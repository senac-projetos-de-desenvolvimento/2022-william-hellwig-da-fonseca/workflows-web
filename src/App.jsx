import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import { ReactNotifications } from 'react-notifications-component';
import React, { useState } from "react";

import { ClienteContext } from "./components/services/ClienteContext";
import UserLogin from "./components/UserLogin";
import Header from "./components/Header";
import Home from "./components/Home";
import Mapa from "./components/Mapa";

import 'react-notifications-component/dist/theme.css';
import 'animate.css';

import CadCompany from "./components/pages/Company/CadCompany";
import Positions from "./components/pages/reports/Positions";
import WorkPoint from "./components/pages/reports/WorkPoint";
import Avisos from "./components/pages/Company/CadWarning";
import CadUsers from "./components/pages/Users/CadUsers";
import Listagem from "./components/pages/Users/Listagem";
import Reports from "./components/pages/Users/Reports";
import Vagas from "./components/pages/Company/CadVags";
import PointsGrafics from "./components/pages/reports/PointsGrafics";

function App() {

  const [dados, setDados] = useState({})

  return (
    <ClienteContext.Provider value={{ dados, setDados }} >
      <Router >
        <Header />
        <ReactNotifications id='notification' />
        <Routes >
          <Route path="/" exact element={<UserLogin />} />
          <Route path="/login" exact element={<UserLogin />} />
          <Route path="/home"  element={<Home />} />
          <Route path="/mapa"  element={<Mapa />} />
          <Route path="/cadusers" element={<CadUsers />} />
          <Route path="/listscore" element={<Listagem />} />
          <Route path="/reports" element={<Reports />} />
          <Route path="/cadcomp" element={<CadCompany />} />
          <Route path="/cadvags" element={<Vagas />} />
          <Route path="/cadwarnings" element={<Avisos />} />
          <Route path="/workpoint" element={<WorkPoint />} />
          <Route path="/positions" element={<Positions />} />
          <Route path="/relgrafics" element={<PointsGrafics />} />
        </Routes>
      </Router>
    </ClienteContext.Provider>
  );
}

export default App;
