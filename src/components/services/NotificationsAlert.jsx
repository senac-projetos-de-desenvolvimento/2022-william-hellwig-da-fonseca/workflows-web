import { Store } from 'react-notifications-component'

const NotificationsAlert = (type, title, subtitle="") => {
  Store.addNotification({
    title: title,
    message: subtitle,
    type: type,
    insert: "top",
    container: "top-right",
    animationIn: ["animate__animated", "animate__fadeIn"],
    animationOut: ["animate__animated", "animate__fadeOut"],
    dismiss: {
      duration: 2000,
    }
  });
};
//Posiçoes
// top-left
// top-right
// top-center
// center
// bottom-left
// bottom-right
// bottom-center

// Variantes
// success
// danger
// info
// default
// warning


export default NotificationsAlert;
