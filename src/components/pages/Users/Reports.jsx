import React, { useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom';
import { GoReport } from "react-icons/go";
import { AiOutlineClose } from "react-icons/ai";
import { BiTrash } from "react-icons/bi";

import NotificationsAlert from '../../services/NotificationsAlert';
import { Button, Image, Modal, NavDropdown } from 'react-bootstrap';
import Conect from '../../../Api/Conect';
import '../../css/style.css'

const Reports = () => {
    const token = localStorage.getItem("token")
    const url = Conect.defaults.baseURL
    const history = useNavigate()
    const [data, setData] = useState([])
    const [show, setShow] = useState(false)
    const [value, setValue] = useState()
    const [valueType, setValueType] = useState()

    useEffect(() => {
        if (token === null) {
            NotificationsAlert("warning", "Desconectado", "Faça login para continuar!")
            history("/login");
        }
        getData()
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);


    const close = async () => {
        setValue()
        setValueType()
        setShow(false)
    }
    const sendRemove = async () => {
        if (valueType === 0) {
            const response = await Conect.delete(`/report/${token}/${value.idReport}`)
            console.log(response.data)
            if (response.data != null) {
                if (response.data.erro === false) {
                    NotificationsAlert("success", response.data.msg, "")
                    getData()
                    close()
                } else {
                    NotificationsAlert("warning", response.data.msg, "")
                }
            }
        } else {
            const response = await Conect.post(`/dellreport/${token}`, {
                idReport: value.idReport,
                type: valueType,
                idPublication: value.reportIdPublication
            })
            if (response.data.erro === false) {
                NotificationsAlert("success", response.data.msg, "")
                getData()
                close()
            } else {
                NotificationsAlert("warning", response.data.msg, "")
            }
        }
    }
    const removeReport = (data, type) => {
        setShow(true)
        setValue(data)
        setValueType(type)
    }
    const getData = async () => {
        const response = await Conect.get(`/report/${token}`)
        if (response.data.data !== null) {
            let values = response.data.data
            setData(values)
        }
    }
    return (
        <div style={{
            width: '100vw',
            height: '180vh',
            position: 'relative',
            backgroundColor: "#272E48"

        }}>
            <Modal show={show}>
                <Modal.Header>
                    {value != null &&
                        valueType === 1 ?
                        <Modal.Title>Remover Ideia?</Modal.Title>
                        :
                        valueType === 3 ?
                            <Modal.Title>Remover Publicação?</Modal.Title>
                            :
                            valueType === 2 ?
                                <Modal.Title>Remover Comentário?</Modal.Title>
                                :
                                <Modal.Title>Remover Denuncia?</Modal.Title>
                    }
                </Modal.Header>
                <Modal.Footer>
                    <Button variant="primary" style={{ backgroundColor: "#354787" }} onClick={() => { sendRemove() }}>
                        Confirmar
                    </Button>
                    <Button variant="secondary" onClick={() => { close() }}>
                        Cancelar
                    </Button>
                </Modal.Footer>
            </Modal>
            <div style={{
                margin: 2,
                justifyContent: 'center',
                alignItems: 'center',
                marginBottom: 20,
                marginTop: 15,
            }}>
                <div style={{
                    marginLeft: 20,
                    display: 'flex',
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'center',
                }}>
                    <h3 style={{ color: '#fff', marginRight: 5 }}>Denuncias</h3>
                    <GoReport style={{ fontSize: 30, color: '#fff' }} />
                </div>

            </div>
            <div>
                {data ?
                    data.map((value, index) => {
                        return (value.type === 1 ?
                            <div style={{ marginBottom: 20 }}>
                                <h3 style={{ marginLeft: 30, color: '#fff' }}>Ideias</h3>
                                <NavDropdown.Divider />
                                <div style={{ marginBottom: 10 }}>
                                    <div style={{
                                        marginLeft: 20,
                                        borderRadius: 10,
                                        backgroundColor: "#354787",
                                        color: '#fff',
                                        width: '95%',
                                        alignItems: 'center',
                                        justifyContent: 'center',
                                        paddingTop: 10,
                                        paddingBottom: 10
                                    }}>
                                        <Button variant='outline-danger' onClick={() => { removeReport(value, 0) }}
                                            style={{ marginLeft: 10, borderRadius: 45 }} ><AiOutlineClose /></Button>
                                        <text style={{ marginLeft: 10 }}>Denuncia: {value.reportDesc}</text>
                                    </div>
                                </div>
                                <div style={{
                                    marginLeft: 20,
                                    borderRadius: 10,
                                    backgroundColor: "#354787",
                                    width: '95%',
                                    alignItems: 'center',
                                    justifyContent: 'center',
                                    paddingBottom: 10
                                }}>

                                    <div style={{
                                        display: 'flex',
                                        flexDirection: 'row',
                                        marginLeft: '1%',
                                        paddingTop: 10,
                                    }}
                                    >

                                        <Button variant='outline-danger' onClick={() => { removeReport(value, 1) }}
                                            style={{ marginRight: 10, borderRadius: 45 }} >
                                            <BiTrash /></Button>
                                        <Image
                                            style={{
                                                width: 40,
                                                height: 40,
                                                borderRadius: 45,
                                                marginRight: 5
                                            }}
                                            src={`${url}${value.image}`} />
                                        <h5 style={{ color: "#fff", marginLeft: '1%' }}>{value.name}</h5>
                                    </div >
                                    <div style={{
                                        marginLeft: '10%',
                                        color: "#fff",
                                        textDecoration: 'underline',
                                        textDecorationColor: '#FF4500'
                                    }}>
                                        {value.suggestion}
                                    </div>
                                </div>

                                <NavDropdown.Divider />
                            </div>
                            :
                            value.type === 3 ?
                                <div style={{ marginBottom: 20 }} >
                                    <h3 style={{ marginLeft: 30, color: '#fff' }}>Publicações</h3>
                                    <NavDropdown.Divider />
                                    <div style={{ marginBottom: 10 }}>
                                        <div style={{
                                            marginLeft: 20,
                                            borderRadius: 10,
                                            backgroundColor: "#354787",
                                            color: '#fff',
                                            width: '95%',
                                            alignItems: 'center',
                                            justifyContent: 'center',
                                            paddingTop: 10,
                                            paddingBottom: 10
                                        }}>
                                            <Button variant='outline-danger' onClick={() => { removeReport(value, 0) }}
                                                style={{ marginLeft: 10, borderRadius: 45 }} ><AiOutlineClose /></Button>
                                            <text style={{ marginLeft: 10 }}>Denuncia: {value.reportDesc}</text>
                                        </div>
                                    </div>
                                    <div style={{
                                        marginLeft: 20,
                                        borderRadius: 10,
                                        backgroundColor: "#354787",
                                        width: '95%',
                                        alignItems: 'center',
                                        justifyContent: 'center',
                                        paddingBottom: 10
                                    }}>

                                        <div style={{
                                            display: 'flex',
                                            flexDirection: 'row',
                                            marginLeft: '1%',
                                            paddingTop: 10,
                                        }}
                                        >
                                            <Button variant='outline-danger' onClick={() => { removeReport(value, 3) }}
                                                style={{ marginRight: 10, borderRadius: 45 }} ><BiTrash /></Button>
                                            <Image
                                                style={{
                                                    width: 40,
                                                    height: 40,
                                                    borderRadius: 45,
                                                    marginRight: 5
                                                }}
                                                src={`${url}${value.image}`} />
                                            <h5 style={{ color: "#fff", marginLeft: '1%' }}>{value.name}</h5>
                                        </div >
                                        <div style={{
                                            marginLeft: '10%',
                                            color: "#fff",
                                            textDecoration: 'underline',
                                            textDecorationColor: '#FF4500'
                                        }}>
                                            {value.publication}
                                        </div>
                                    </div>

                                    <NavDropdown.Divider />
                                </div >
                                :
                                value.type === 2 &&
                                <div style={{ marginBottom: 20 }}>
                                    <h3 style={{ marginLeft: 30, color: '#fff' }}>Comentários</h3>
                                    <NavDropdown.Divider />
                                    <div style={{ marginBottom: 10 }}>
                                        <div style={{
                                            marginLeft: 20,
                                            borderRadius: 10,
                                            backgroundColor: "#354787",
                                            color: '#fff',
                                            width: '95%',
                                            alignItems: 'center',
                                            justifyContent: 'center',
                                            paddingTop: 10,
                                            paddingBottom: 10
                                        }}>
                                            <Button variant='outline-danger' onClick={() => { removeReport(value, 0) }}
                                                style={{ marginLeft: 10, borderRadius: 45 }} ><AiOutlineClose /></Button>
                                            <text style={{ marginLeft: 10 }}>Denuncia: {value.reportDesc}</text>
                                        </div>
                                    </div>
                                    <div style={{
                                        marginLeft: 20,
                                        borderRadius: 10,
                                        backgroundColor: "#354787",
                                        width: '95%',
                                        alignItems: 'center',
                                        justifyContent: 'center',
                                        paddingBottom: 10
                                    }}>

                                        <div style={{
                                            display: 'flex',
                                            flexDirection: 'row',
                                            marginLeft: '2%',
                                            paddingTop: 10,
                                        }}
                                        >
                                            <Image
                                                style={{
                                                    width: 40,
                                                    height: 40,
                                                    borderRadius: 45,
                                                    marginRight: 5
                                                }}
                                                src={`${url}${value.image}`} />
                                            <h5 style={{ color: "#fff", marginLeft: '1%' }}>{value.name}</h5>
                                        </div >
                                        <div style={{
                                            marginLeft: '7%',
                                            marginTop: -10,
                                            color: "#fff",
                                        }}>
                                            {value.publication}
                                        </div>
                                        {value.comment.map((values, i) => {
                                            return (
                                                values.idComment === value.reportIdPublication ?
                                                    <>
                                                        <div style={{
                                                            display: 'flex',
                                                            flexDirection: 'row',
                                                            marginLeft: '1%',
                                                            paddingTop: 15,
                                                        }}
                                                        >
                                                            <Button variant='outline-danger' onClick={() => { removeReport(values, 2) }}
                                                                style={{ marginRight: 18, borderRadius: 45 }} ><BiTrash /></Button>
                                                            <Image
                                                                style={{
                                                                    width: 30,
                                                                    height: 30,
                                                                    borderRadius: 45,
                                                                    marginRight: 5
                                                                }}
                                                                src={`${url}${values.image}`} />
                                                            <h5 style={{ color: "#fff", marginLeft: '1%' }}>{values.name}</h5>
                                                        </div >
                                                        <div style={{
                                                            marginLeft: '9%',
                                                            color: "#fff",
                                                            textDecoration: 'underline',
                                                            textDecorationColor: '#FF4500',
                                                            marginTop: -5,
                                                        }}>
                                                            {values.comment}
                                                        </div>
                                                    </>
                                                    :
                                                    <>

                                                        <div style={{
                                                            display: 'flex',
                                                            flexDirection: 'row',
                                                            marginLeft: '5%',
                                                            paddingTop: 20,
                                                        }}
                                                        >
                                                            <Image
                                                                style={{
                                                                    width: 30,
                                                                    height: 30,
                                                                    borderRadius: 45,
                                                                    marginRight: 5
                                                                }}
                                                                src={`${url}${values.image}`} />
                                                            <h5 style={{ color: "#fff", marginLeft: '1%' }}>{values.name}</h5>
                                                        </div >
                                                        <div style={{
                                                            marginLeft: '9%',
                                                            marginTop: -5,
                                                            color: "#fff",
                                                        }}>
                                                            {values.comment}
                                                        </div>
                                                    </>
                                            )
                                        })}
                                    </div>
                                    <NavDropdown.Divider />
                                </div>
                        )
                    })
                    :
                    <div style={{
                        marginLeft: 20,
                        display: 'flex',
                        flexDirection: 'row',
                        alignItems: 'center',
                        justifyContent: 'center',
                    }}>
                        <h2 style={{ color: '#fff' }}>Sem Denuncias</h2>
                    </div>
                }
            </div>
        </div >
    );
}

export default Reports;