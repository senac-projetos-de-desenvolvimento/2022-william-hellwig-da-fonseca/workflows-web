import React, { useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom';
import { GiPodium } from "react-icons/gi";
import { AiTwotoneCrown } from "react-icons/ai";

import { Element } from 'react-scroll';

import NotificationsAlert from '../../services/NotificationsAlert';
import { Button, Form, Image, InputGroup, Modal, Table } from 'react-bootstrap';
import Conect from '../../../Api/Conect';
import '../../css/style.css'
import { useForm } from 'react-hook-form';

const Listagem = () => {
    const token = localStorage.getItem("token")
    const url = Conect.defaults.baseURL
    const history = useNavigate()
    const [data, setData] = useState([])
    const [users, setUsers] = useState([])
    const [show, setShow] = useState(false)
    const { register, handleSubmit, setValue } = useForm()

    useEffect(() => {
        if (token === null) {
            NotificationsAlert("warning", "Desconectado", "Faça login para continuar!")
            history("/login");
        } else {
            getData()
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);


    const newRanking = async () => {
        setShow(true)
    }
    const getData = async () => {
        const response = await Conect.get(`/score/${token}`)
        if (response.data.Users !== null) {
            let values = response.data.Users
            let dados = values.map((value, index) => {
                return (<tr style={{ color: "#fff" }} key={index}>
                    <td>{
                        index === 0 ? <AiTwotoneCrown style={{ color: "#FFD700", fontSize: 25 }} />
                            :
                            index === 1 ? <AiTwotoneCrown style={{ color: "#C0C0C0", fontSize: 25 }} />
                                :
                                index === 2 ? <AiTwotoneCrown style={{ color: "#cd7f32", fontSize: 25 }} />
                                    :
                                    index + 1}
                    </td>
                    <td>
                        <Image
                            style={{
                                width: 25,
                                height: 25,
                                borderRadius: 45, marginRight: 5
                            }}
                            src={`${url}${value.image}`} />
                        {value.name}</td>
                    <td>{value.score}</td>
                </tr>)
            })
            let select = values.map((value, index) => {
                return (
                    <option key={index} value={value.id}>{value.name}</option>
                )
            })
            setData(dados)
            setUsers(select)
        }
    }

    const onSubmit = async (data) => {
        const response = await Conect.put(`/userupscore/${token}`, {
            id: Number(data.id),
            score: Number(data.score)
        })
        if (response.data != null) {
            if (response.data.erro === false) {
                closeEdit();
                NotificationsAlert("success", response.data.msg, "")
                getData()
            } else {
                NotificationsAlert("warning", response.data.msg, "")
            }
        }
        setShow(false)
    }
    const closeEdit = () => {
        setValue('id', 2606)
        setValue('score', null)
        setShow(false)
    }

    return (
        <div style={{
            width: '100vw',
        }}>
            <Modal show={show} >
                <form onSubmit={handleSubmit(onSubmit)}>

                    <Modal.Header>
                        <Modal.Title>Novo Ranking</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <InputGroup className="mb-3">
                            <InputGroup.Text>Usuario</InputGroup.Text>
                            <Form.Select {...register("id")} >
                                <option>Selecione</option>
                                <option value="2606">Todos</option>
                                {users}
                            </Form.Select>
                        </InputGroup>
                        <InputGroup className="mb-3">
                            <InputGroup.Text>Pontos</InputGroup.Text>
                            <Form.Control type='number' {...register("score")}
                            />
                        </InputGroup>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button
                            type="submit"
                            style={{ backgroundColor: '#354787', borderRadius: 45 }}
                            onSubmit={() => { handleSubmit(onSubmit) }} variant="primary" >
                            Salvar
                        </Button>
                        <Button
                            style={{ borderRadius: 45 }}
                            onClick={() => { closeEdit() }}
                            variant="secondary" >
                            Cancelar
                        </Button>
                    </Modal.Footer>
                </form>
            </Modal>
            <div style={{
                margin: 2,
                justifyContent: 'center',
                alignItems: 'center',
                marginBottom: 20,
                marginTop: 15,
            }}>
                <div style={{
                    marginLeft: 20,
                    display: 'flex',
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'center',
                }}>
                    <GiPodium style={{ fontSize: 30, marginBottom: 10, color: '#fff' }} />
                    <h3 style={{ color: '#fff', marginLeft: 5, marginRight: "15%" }}>Ranking</h3>
                </div>

            </div>
            <div style={{
                display: 'flex',
                flexWrap: 'wrap',
                justifyContent: 'center',
                flexDirection: 'row',
            }}>

                <div style={{
                    backgroundColor: '#354787',
                    width: "50%",
                    height: "78vh"

                }}>
                    <Element name="test7" className="element" id="containerElement" style={{
                        position: 'relative',
                        height: '100%',
                        width: '100%',
                        overflow: 'scroll',
                    }}>
                        <Table >
                            <thead>
                                <tr style={{ color: '#fff' }}>
                                    <th><text>Posição</text></th>
                                    <th><text>Nome</text></th>
                                    <th><text>Pontos</text></th>
                                </tr>
                            </thead>

                            <tbody>
                                {data}
                            </tbody>
                        </Table>
                    </Element>
                </div>
                <Button
                    style={{ width: "10%", height: "10%", borderRadius: 45, marginLeft: 10, backgroundColor: '#354787', color: "#fff" }}
                    variant="outline-light"
                    onClick={() => { newRanking() }}
                >
                    {/* <BsPlusLg style={{ marginRight: 5, marginBottom: 3 }} /> */}
                    <text>Novo Ranking</text></Button>
            </div>
        </div >
    );
}

export default Listagem;