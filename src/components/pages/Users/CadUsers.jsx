import React, { useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom';
import { FaPen } from "react-icons/fa";
import { BsCheck2, BsDash, BsPlusLg } from "react-icons/bs";

import { Element } from 'react-scroll';

import NotificationsAlert from '../../services/NotificationsAlert';
import { Button, Col, Container, Form, Image, InputGroup, Modal, Row, Table } from 'react-bootstrap';
import Conect from '../../../Api/Conect';
import moment from 'moment/moment';
import '../../css/style.css'
import { useForm } from 'react-hook-form';

const CadUsers = () => {
    const token = localStorage.getItem("token")
    const url = Conect.defaults.baseURL
    const history = useNavigate()
    const [data, setData] = useState([])
    const [show, setShow] = useState(false)
    const [typeSubmit, setTypeSubmit] = useState(false)
    const [positions, setPositions] = useState(null)
    const { register, handleSubmit, setValue } = useForm()

    useEffect(() => {
        if (token === null) {
            NotificationsAlert("warning", "Desconectado", "Faça login para continuar!")
            history("/login");
        } else {
            getData()
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const getPositions = async () => {
        const response = await Conect.get(`/job/${token}`)
        if (response.data.data !== null) {

            let opt = response.data.data.map((value, index) => {
                return (<option key={index} value={value.id}>{value.position}</option>)
            })
            setPositions(opt)
        }
    }
    const getData = async () => {
        const response = await Conect.get(`/user/${token}`)
        if (response.data.Users !== null) {
            let values = response.data.Users
            let dados = values.map((value, index) => {
                return (<tr style={{ color: "#fff" }} key={index}>
                    <td>{index + 1}</td>
                    <td>{value.status === 1 ?
                        <BsCheck2 style={{ fontSize: 30, color: '#0f0' }} />
                        :
                        <BsDash style={{ fontSize: 30, color: '#A9A9A9' }} />}</td>
                    <td>
                        <Image
                            style={{
                                width: 25,
                                height: 25,
                                borderRadius: 45, marginRight: 5
                            }}
                            src={`${url}${value.image}`} />
                        {value.name}</td>
                    <td>{value.email}</td>
                    <td>{value.position}</td>
                    <td>{value.fone}</td>
                    <td>{value.ctps}</td>
                    <td style={{ textAlign: 'center' }}>
                        {value.location === 1 ?
                            <BsCheck2 style={{ fontSize: 30, color: '#0f0' }} />
                            :
                            <BsDash style={{ fontSize: 30, color: '#A9A9A9' }} />}</td>
                    <td>{moment(value.created_at).format("DD-MM-YYYY")}</td>
                    <td style={{ textAlign: 'center', color: '#FD9226' }}>
                        <Button
                            style={{ borderRadius: 45, fontSize: 12 }}
                            variant="outline-warning"
                            onClick={() => { editUser(value) }}><FaPen /></Button></td>
                </tr>)
            })
            setData(dados)
            getPositions()
        }

    }
    const createUser = () => {
        setValue('location', "2606")
        setValue('job_position_id', "2606")
        setTypeSubmit(true)
        setShow(true)
    }
    const editUser = (userProps) => {
        // setUserProps(userProps)
        setValue('id', userProps.id)
        setValue('name', userProps.name)
        setValue('email', userProps.email)
        setValue('fone', userProps.fone)
        setValue('ctps', userProps.ctps)
        setValue('job_position_id', userProps.job_position_id)
        setValue('location', userProps.location)
        setValue('password', null)
        setShow(true)
    }
    const closeEdit = () => {
        setValue('id', null)
        setValue('name', null)
        setValue('email', null)
        setValue('fone', null)
        setValue('ctps', null)
        setValue('job_position_id', null)
        setValue('location', null)
        setValue('password', null)
        setTypeSubmit(false)
        setShow(false)
    }
    const onSubmit = async (data) => {
        if (typeSubmit === true) {
            const response = await Conect.post(`/user/${token}`, {
                name: data.name,
                email: data.email,
                fone: data.fone,
                ctps: data.ctps,
                password: data.password,
                location: data.location,
                job_position_id: data.job_position_id,
                status: data.status
            })
            if (response.data != null) {
                if (response.data.erro === false) {
                    closeEdit();
                    NotificationsAlert("success", response.data.msg, "")
                    setTypeSubmit(false)
                    getData()
                } else {
                    NotificationsAlert("danger", response.data.msg, "")
                }
            }
        } else {
            const response = await Conect.post(`/userupjob/${token}`, {
                id: data.id,
                name: data.name,
                email: data.email,
                fone: data.fone,
                ctps: data.ctps,
                newPassword: data.password,
                location: data.location,
                job_position_id: data.job_position_id,
                status: data.status
            })
            if (response.data != null) {
                if (response.data.erro === false) {
                    closeEdit();
                    NotificationsAlert("success", response.data.msg, "")
                    setTypeSubmit(false)
                    getData()
                } else {
                    NotificationsAlert("warning", response.data.msg, "")
                }
            }
        }
    }

    return (
        <div style={{
            width: '100vw',

        }}>
            <div>
                <Modal
                    show={show}
                    aria-labelledby="contained-modal-title-vcenter"
                    centered
                    dialogClassName="modal-90w"
                >
                    <form onSubmit={handleSubmit(onSubmit)}>
                        <Modal.Header>
                            <Modal.Title id="contained-modal-title-vcenter">
                                Cadastro do Colaborador.
                            </Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                            <Container>
                                <Row>
                                    <Col>
                                        <InputGroup className="mb-3">
                                            <InputGroup.Text>Nome</InputGroup.Text>
                                            <Form.Control {...register("name")}
                                            />
                                        </InputGroup>
                                    </Col>
                                    <Col>
                                        <InputGroup className="mb-3">
                                            <InputGroup.Text>Email</InputGroup.Text>
                                            <Form.Control
                                                type='email'
                                                {...register("email")}
                                            />
                                        </InputGroup>
                                    </Col>
                                    <Col>
                                        <InputGroup className="mb-3">
                                            <InputGroup.Text>Fone</InputGroup.Text>
                                            <Form.Control
                                                {...register("fone")}
                                            />
                                        </InputGroup>
                                    </Col>

                                </Row>
                                <Row>
                                    <Col>
                                        <InputGroup className="mb-3">
                                            <InputGroup.Text>CTPS</InputGroup.Text>
                                            <Form.Control
                                                {...register("ctps")}
                                            />
                                        </InputGroup>
                                    </Col>
                                    <Col>
                                        <InputGroup className="mb-3">
                                            <InputGroup.Text>Cargo</InputGroup.Text>
                                            <Form.Select {...register("job_position_id")} >
                                                <option value="2606">Selecione</option>
                                                {positions}
                                            </Form.Select>
                                        </InputGroup>
                                    </Col>
                                    <Col>
                                        <InputGroup className="mb-3">
                                            <InputGroup.Text>Localização</InputGroup.Text>
                                            <Form.Select {...register("location")} >
                                                <option value="2606">Selecione</option>
                                                <option value="1">Sim</option>
                                                <option value="0">Não</option>
                                            </Form.Select>
                                        </InputGroup>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col>
                                        <InputGroup className="mb-3">
                                            <InputGroup.Text>Senha</InputGroup.Text>
                                            <Form.Control
                                                type='password'
                                                placeholder='***********'
                                                {...register("password")}
                                            />
                                        </InputGroup>
                                    </Col>
                                    <Col>
                                        <InputGroup className="mb-3">
                                            <InputGroup.Text>Status</InputGroup.Text>
                                            <Form.Select {...register("status")} >
                                                <option value="1">Ativo</option>
                                                <option value="0">Inativo</option>
                                            </Form.Select>
                                        </InputGroup>
                                    </Col>
                                </Row>
                            </Container>

                        </Modal.Body>
                        <Modal.Footer>
                            <Button type="submit" onSubmit={() => { handleSubmit(onSubmit) }}>Salvar</Button>
                            <Button onClick={() => { closeEdit() }}>Fechar</Button>
                        </Modal.Footer>
                    </form>
                </Modal>
            </div>
            <div style={{
                margin: 2,
                justifyContent: 'center',
                alignItems: 'center',
                marginBottom: 20,
                marginTop: 15,
            }}>
                <Container>
                    <Row>
                        <Col>
                            <h3 style={{ color: '#fff' }} >Cadastro de Colaboradores</h3>
                        </Col>
                        <Col style={{ justifyContent: "center", alignItems: "center" }}>
                            <Button
                                style={{ width: "90%", borderRadius: 45 }}
                                variant="outline-success"
                                onClick={() => { createUser() }}>
                                    <BsPlusLg style={{ marginRight: 5, marginBottom: 3 }} />
                                    <text>Adicionar Colaborador</text></Button>
                        </Col>
                    </Row>
                </Container>
            </div>
            <div style={{
                display: 'flex',
                flexWrap: 'wrap',
                justifyContent: 'center',
                flexDirection: 'row',
            }}>
                <div style={{
                    backgroundColor: '#354787',
                    width: "95%",
                    height: "78vh"

                }}>
                    <Element name="test7" className="element" id="containerElement" style={{
                        position: 'relative',
                        height: '100%',
                        width: '100%',
                        overflow: 'scroll',
                    }}>
                        <Table >
                            <thead>
                                <tr style={{ color: '#fff' }}>
                                    <th><text>#</text></th>
                                    <th><text>Status</text></th>
                                    <th><text>Nome</text></th>
                                    <th><text>Email</text></th>
                                    <th><text style={{ marginRight: '10vw' }}>Cargo</text></th>
                                    <th><text style={{ marginRight: '5vw' }}>Contato</text></th>
                                    <th><text>Ctps</text></th>
                                    <th><text >Localização</text></th>
                                    <th><text style={{ marginRight: '5vw' }}>Criação</text></th>
                                    <th>Editar</th>
                                </tr>
                            </thead>

                            <tbody>
                                {data}
                            </tbody>
                        </Table>
                    </Element>
                </div>
            </div>
        </div>
    );
}

export default CadUsers;