import { Button, Col, Container, Form, InputGroup, Modal, Row, Table } from 'react-bootstrap';
import React, { useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom';
import { useForm } from 'react-hook-form';
import { FaPencilAlt } from "react-icons/fa";
import { BiTrash } from "react-icons/bi";
import { Element } from 'react-scroll';

import NotificationsAlert from '../../services/NotificationsAlert';
import Conect from '../../../Api/Conect';
import '../../css/style.css'
import moment from 'moment/moment';

const Avisos = () => {
    const token = localStorage.getItem("token")
    const history = useNavigate()
    const [data, setData] = useState([])
    const [show, setShow] = useState(false)
    const [newCad, setNewCad] = useState(false)
    const [showDelete, setShowDelete] = useState(false)
    const { register, handleSubmit, setValue, watch } = useForm()

    useEffect(() => {
        if (token === null) {
            NotificationsAlert("warning", "Desconectado", "Faça login para continuar!")
            history("/login");
        } else {
            getData()
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const getData = async () => {
        const resVac = await Conect.get(`/warning/${token}`)
        if (resVac.data.warnings != null) {
            let values = resVac.data.warnings
            let dados = values.map((value, index) => {
                return (
                    <tr key={index}>
                        <td>
                            {value.weight === 1 ?
                                <div style={{ backgroundColor: '#FF0000', width: '20vw', borderRadius: 45 }}>
                                    <h3 style={{ textAlign: 'center' }}>{value.title}</h3>
                                </div>
                                :
                                value.weight === 2 ?
                                    <div style={{ backgroundColor: '#FFFF00', width: '20vw', borderRadius: 45 }}>
                                        <h3 style={{ textAlign: 'center', color: '#000' }}>{value.title}</h3>
                                    </div>
                                    :
                                    value.weight === 3 &&
                                    <div style={{ backgroundColor: '#00BFFF', width: '20vw', borderRadius: 45 }}>
                                        <h3 style={{ textAlign: 'center' }}>{value.title}</h3>
                                    </div>
                            }
                            <p style={{ fontStyle: 'italic', fontSize: 12 }}>
                                {moment(value.created_at).format('HH:MM')} {moment(value.created_at).format('DD/MM/YY')}
                            </p>
                            <p>{value.description}</p>
                        </td>
                        <td style={{
                            display: 'flex',
                            justifyContent: 'center',
                            alignItems: 'center',
                            height: '100%'
                        }}>
                            <Button
                                onClick={() => { cad(value, 0) }}
                                variant='outline-warning'
                                style={{ borderRadius: 45 }}>
                                <FaPencilAlt />
                            </Button></td>
                        <td style={{
                            display: 'flex',
                            justifyContent: 'center',
                            alignItems: 'center'
                        }}>
                            <Button
                                onClick={() => { openDell(value) }}
                                variant='outline-danger'
                                style={{ borderRadius: 45 }}>
                                <BiTrash />
                            </Button></td>
                    </tr >
                )
            })
            setData(dados)
        } else {
            getData()
        }
    }

    const openDell = (data) => {
        setValue('id', data.idwarning)
        setShowDelete(true)
    }

    const cad = async (data, type) => {
        if (type === 0) {
            setValue('id', data.idwarning)
            setValue('weight', data.weight)
            setValue('title', data.title)
            setValue('description', data.description)
            setShow(true)
        } else {
            setValue('weight', 2606)
            setValue('title', null)
            setValue('description', null)
            setNewCad(true)
            setShow(true)
        }
    }

    const closeEdit = () => {

        setValue('id', null)
        setValue('weight', null)
        setValue('title', null)
        setValue('description', null)
        setShow(false)
        setShowDelete(false)
        setNewCad(false)
    }

    const onSubmit = async (data) => {
        if (newCad === true) {
            const response = await Conect.post(`/warning/${token}`, {
                weight: data.weight,
                title: data.title,
                description: data.description
            })
            if (response.data != null) {
                if (response.data.erro === false) {
                    NotificationsAlert("success", response.data.msg, "")
                    closeEdit();
                    getData()
                } else {
                    NotificationsAlert("warning", response.data.msg, "")
                }
            }
        } else {
            const response = await Conect.put(`/warning/${token}`, {
                id: data.id,
                weight: data.weight,
                title: data.title,
                description: data.description
            })
            if (response.data != null) {
                if (response.data.erro === false) {
                    NotificationsAlert("success", response.data.msg, "")
                    closeEdit();
                    getData()
                } else {
                    NotificationsAlert("warning", response.data.msg, "")
                }
            }
        }
    }

    const sendRemove = async () => {
        const response = await Conect.delete(`/delwarning/${token}/${watch("id")}`)
        console.log(response)
        if (response.data.erro === false) {
            NotificationsAlert("success", response.data.msg, "")
            getData()
            closeEdit()
        } else {
            NotificationsAlert("warning", response.data.msg, "")
            closeEdit()
        }
    }

    return (
        <div style={{
            width: '100vw',
        }}>
            <Modal show={show} >
                <form onSubmit={handleSubmit(onSubmit)}>
                    <Modal.Header>
                        <Modal.Title>Gerenciamento de Vaga</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <InputGroup className="mb-3">
                            <InputGroup.Text>Importançia</InputGroup.Text>
                            <Form.Select {...register("weight")}>
                                <option value={2606}>Selecione</option>
                                <option value={1}>Importante</option>
                                <option value={2}>Aviso</option>
                                <option value={3}>Informativo</option>
                            </Form.Select>
                        </InputGroup>

                        <InputGroup className="mb-3">
                            <InputGroup.Text>Titulo</InputGroup.Text>
                            <Form.Control
                                placeholder='Informe o titulo...'
                                {...register("title")}
                            />
                        </InputGroup>
                        <InputGroup className="mb-3">
                            <InputGroup.Text>Descrição</InputGroup.Text>
                            <Form.Control
                                placeholder='Descrição do aviso...'
                                as="textarea"
                                rows={3}
                                {...register("description")}
                            />
                        </InputGroup>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button
                            type="submit"
                            style={{ backgroundColor: '#354787', borderRadius: 45 }}
                            onSubmit={() => { handleSubmit(onSubmit) }} variant="primary" >
                            Salvar
                        </Button>
                        <Button
                            style={{ borderRadius: 45 }}
                            onClick={() => { closeEdit() }}
                            variant="secondary" >
                            Cancelar
                        </Button>
                    </Modal.Footer>
                </form>
            </Modal>

            <Modal show={showDelete}>
                <Modal.Header>
                    <Modal.Title>Remover Aviso?</Modal.Title>
                </Modal.Header>
                <Modal.Footer>
                    <Button variant="primary" style={{ backgroundColor: "#354787" }}
                        onClick={() => { sendRemove() }}
                    >
                        Confirmar
                    </Button>
                    <Button variant="secondary" onClick={() => { closeEdit() }}>
                        Cancelar
                    </Button>
                </Modal.Footer>
            </Modal>

            <div style={{
                margin: 2,
                justifyContent: 'center',
                alignItems: 'center',
                marginBottom: 20,
                marginTop: 15,
            }}>
                <Container>
                    <Row>
                        <Col>
                            <h3 style={{ color: '#fff' }} >Avisos Cadastrados</h3>
                        </Col>
                        <Col style={{ justifyContent: "center", alignItems: "center" }}>
                            <Button
                                style={{ width: "90%", borderRadius: 45 }}
                                variant="outline-success"
                                onClick={() => { cad(0, 1) }}
                            >
                                {/* <BsPlusLg style={{ marginRight: 5, marginBottom: 3 }} /> */}
                                <text>Adicionar Aviso</text></Button>
                        </Col>
                    </Row>
                </Container>
            </div>
            <div style={{
                display: 'flex',
                flexWrap: 'wrap',
                justifyContent: 'center',
                flexDirection: 'row',
            }}>
                <div style={{
                    backgroundColor: '#354787',
                    width: "95%",
                    height: "78vh"

                }}>
                    <Element name="test7" className="element" id="containerElement" style={{
                        position: 'relative',
                        height: '100%',
                        width: '100%',
                        overflow: 'scroll',
                    }}>
                        <Table style={{ color: "#fff" }} bordered >
                            <thead>
                                <tr>
                                    <th>Avisos</th>
                                    <th>Editar/Remover</th>
                                </tr>
                            </thead>
                            <tbody>
                                {data}
                            </tbody>
                        </Table>
                    </Element>
                </div>
            </div>
        </div >
    );
}

export default Avisos;