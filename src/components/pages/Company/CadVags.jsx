import { Button, Col, Container, Form, InputGroup, Modal, Row, Table } from 'react-bootstrap';
import React, { useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom';
import { useForm } from 'react-hook-form';
import { FaPencilAlt } from "react-icons/fa";
import { BiTrash } from "react-icons/bi";
import { Element } from 'react-scroll';

import NotificationsAlert from '../../services/NotificationsAlert';
import Conect from '../../../Api/Conect';
import '../../css/style.css'
import moment from 'moment/moment';

const Vagas = () => {
    const token = localStorage.getItem("token")
    const userId = localStorage.getItem("user_id")
    const history = useNavigate()
    const [position, setPosition] = useState()
    const [data, setData] = useState([])
    const [show, setShow] = useState(false)
    const [newCad, setNewCad] = useState(false)
    const [showDelete, setShowDelete] = useState(false)
    const { register, handleSubmit, setValue, watch } = useForm()

    useEffect(() => {
        if (token === null) {
            NotificationsAlert("warning", "Desconectado", "Faça login para continuar!")
            history("/login");
        } else {
            getData()
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const getData = async () => {
        const resVac = await Conect.get(`/vacancie/${token}`)
        const resPos = await Conect.get(`/job/${token}`)
        if (resVac.data.data != null && resPos.data.data != null) {
            let values = resVac.data.data
            let valuesPos = resPos.data.data
            let dados = values.map((value, index) => {
                return (
                    <tr key={index}>
                        <td><h3>{value.position}</h3>
                            <p style={{ fontStyle: 'italic', fontSize: 12 }}>
                                {moment(value.created_at).format('HH:MM')} {moment(value.created_at).format('DD/MM/YY')}
                            </p>
                            <p>{value.message}</p></td>
                        <td style={{
                            display: 'flex',
                            justifyContent: 'center',
                            alignItems: 'center'
                        }}>
                            <Button
                                onClick={() => { cad(value, 0) }}
                                variant='outline-warning'
                                style={{ borderRadius: 45 }}>
                                <FaPencilAlt />
                            </Button></td>
                        <td style={{
                            display: 'flex',
                            justifyContent: 'center',
                            alignItems: 'center'
                        }}>
                            <Button
                                onClick={() => { openDell(value) }}
                                variant='outline-danger'
                                style={{ borderRadius: 45 }}>
                                <BiTrash />
                            </Button></td>
                    </tr>
                )
            })
            let positions = valuesPos.map((value, index) => {
                return (
                    <option key={index} value={value.id}>{value.position}</option>
                )
            })
            setData(dados)
            setPosition(positions)

        } else {
            getData()
        }
    }

    const openDell = (data) => {
        setValue('id', data.availablejob_id)
        setShowDelete(true)
    }

    const cad = async (data, type) => {
        if (type === 0) {
            setValue('id', data.availablejob_id)
            setValue('position', data.id)
            setValue('message', data.message)
            setShow(true)
        } else {
            setValue('position', 2606)
            setValue('message', null)
            setNewCad(true)
            setShow(true)
        }
    }

    const closeEdit = () => {
        setValue('position', null)
        setValue('description', null)
        setShow(false)
        setShowDelete(false)
        setNewCad(false)
    }

    const onSubmit = async (data) => {
        if (newCad === true) {
            const response = await Conect.post(`/vacancie/${token}`, {
                idUser: userId,
                idJob: data.position,
                message: data.message
            })
            if (response.data != null) {
                if (response.data.erro === false) {
                    NotificationsAlert("success", response.data.msg, "")
                    closeEdit();
                    getData()
                } else {
                    NotificationsAlert("warning", response.data.msg, "")
                }
            }
        } else {
            const response = await Conect.put(`/vacancie/${token}`, {
                id: data.id,
                idJob: data.position,
                message: data.message
            })
            if (response.data != null) {
                if (response.data.erro === false) {
                    NotificationsAlert("success", response.data.msg, "")
                    closeEdit();
                    getData()
                } else {
                    NotificationsAlert("warning", response.data.msg, "")
                }
            }
        }
    }

    const sendRemove = async () => {
        const response = await Conect.delete(`/delvacancie/${token}/${watch("id")}`)
        if (response.data.erro === false) {
            NotificationsAlert("success", response.data.msg, "")
            getData()
            closeEdit()
        } else {
            NotificationsAlert("warning", response.data.msg, "")
            closeEdit()
        }
    }

    return (
        <div style={{
            width: '100vw',
        }}>
            <Modal show={show} >
                <form onSubmit={handleSubmit(onSubmit)}>
                    <Modal.Header>
                        <Modal.Title>Gerenciamento de Vaga</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <InputGroup className="mb-3">
                            <InputGroup.Text>Cargo</InputGroup.Text>
                            <Form.Select {...register("position")}>
                                <option value={2606}>Selecione</option>
                                {position}
                            </Form.Select>
                        </InputGroup>

                        <InputGroup className="mb-3">
                            <InputGroup.Text>Descrição</InputGroup.Text>
                            <Form.Control
                                placeholder='Breve Descrição Sobre o Cargo...'
                                as="textarea"
                                rows={3}
                                {...register("message")}
                            />
                        </InputGroup>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button
                            type="submit"
                            style={{ backgroundColor: '#354787', borderRadius: 45 }}
                            onSubmit={() => { handleSubmit(onSubmit) }} variant="primary" >
                            Salvar
                        </Button>
                        <Button
                            style={{ borderRadius: 45 }}
                            onClick={() => { closeEdit() }}
                            variant="secondary" >
                            Cancelar
                        </Button>
                    </Modal.Footer>
                </form>
            </Modal>
            <Modal show={showDelete}>
                <Modal.Header>
                    <Modal.Title>Remover Vaga?</Modal.Title>
                </Modal.Header>
                <Modal.Footer>
                    <Button variant="primary" style={{ backgroundColor: "#354787" }}
                        onClick={() => { sendRemove() }}
                    >
                        Confirmar
                    </Button>
                    <Button variant="secondary" onClick={() => { closeEdit() }}>
                        Cancelar
                    </Button>
                </Modal.Footer>
            </Modal>
            <div style={{
                margin: 2,
                justifyContent: 'center',
                alignItems: 'center',
                marginBottom: 20,
                marginTop: 15,
            }}>
                <Container>
                    <Row>
                        <Col>
                            <h3 style={{ color: '#fff' }} >Vagas Abertas</h3>
                        </Col>
                        <Col style={{ justifyContent: "center", alignItems: "center" }}>
                            <Button
                                style={{ width: "90%", borderRadius: 45 }}
                                variant="outline-success"
                                onClick={() => { cad(0, 1) }}
                            >
                                {/* <BsPlusLg style={{ marginRight: 5, marginBottom: 3 }} /> */}
                                <text>Adicionar Vaga</text></Button>
                        </Col>
                    </Row>
                </Container>
            </div>
            <div style={{
                display: 'flex',
                flexWrap: 'wrap',
                justifyContent: 'center',
                flexDirection: 'row',
            }}>
                <div style={{
                    backgroundColor: '#354787',
                    width: "95%",
                    height: "78vh"

                }}>
                    <Element name="test7" className="element" id="containerElement" style={{
                        position: 'relative',
                        height: '100%',
                        width: '100%',
                        overflow: 'scroll',
                    }}>
                        <Table style={{ color: "#fff" }} bordered >
                            <thead>
                                <tr>
                                    <th>Vagas</th>
                                    <th>Editar/Remover</th>
                                </tr>
                            </thead>
                            <tbody>
                                {data}
                            </tbody>
                        </Table>
                    </Element>
                </div>
            </div>
        </div >
    );
}

export default Vagas;