import { GoogleMap, Marker, Circle, useJsApiLoader } from '@react-google-maps/api';
import { Button, Col, Container, Form, InputGroup, Modal, Row, Table } from 'react-bootstrap';
import React, { useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom';
import { useForm } from 'react-hook-form';
import { Element } from 'react-scroll';

import { AiFillSetting, AiOutlinePlus } from "react-icons/ai";
import { FaPencilAlt } from "react-icons/fa";
import { BiTrash } from "react-icons/bi";

import NotificationsAlert from '../../services/NotificationsAlert';
import Conect from '../../../Api/Conect';
import pin from "../../../assets/marker.png";
import '../../css/style.css'


const CadCompany = () => {
    const history = useNavigate()
    const token = localStorage.getItem("token")
    const { isLoaded } = useJsApiLoader({
        id: 'google-map-script',
        googleMapsApiKey: "AIzaSyA-S2vsMYvxkccVcjdk5tC0yXQDwdq7_ZM"
    })

    const { register, handleSubmit, setValue, watch } = useForm()
    const [data, setData] = useState({ lat: -31.769899294545382, lng: -52.340982621306615, zoom: 12 })
    const [jobs, setJobs] = useState([])
    const [listAccess, setListAccess] = useState()
    const [newPosition, setNewPosition] = useState(null)
    const [distance, setDistance] = useState(null)
    const [showDelete, setShowDelete] = useState(false)
    const [show, setShow] = useState(false)
    const [cad, setCad] = useState(false)
    const [edit, setEdit] = useState(false)
    const [submit, setSubmit] = useState(false)


    useEffect(() => {
        if (token === null) {
            NotificationsAlert("warning", "Desconectado", "Faça login para continuar!")
            history("/login");
        }
        getData()
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const editLocation = async () => {
        setEdit(true)
        setNewPosition(null)
    }

    const newLocation = async (lat, lng) => {
        if (edit === true) {
            setNewPosition({ lat: lat, lng: lng })
            setData({ lat: lat, lng: lng })
            setSubmit(true)
        }
    }

    const onSubmit = async (data) => {
        const response = await Conect.put(`/company/${token}`, {
            lat: newPosition.lat,
            lon: newPosition.lng,
            distance: data.distance
        })
        if (response.data != null) {
            if (response.data.erro === false) {
                NotificationsAlert("success", response.data.msg, "")
                getData()
                close()
            } else {
                NotificationsAlert("warning", response.data.msg, "")
                close()
            }
        }
    }

    const close = async () => {
        setValue('positionId', null)
        setValue('description', null)
        setValue('access', null)
        setShowDelete(false)
        setShow(false)
        setCad(false)
        setEdit(false)
        setSubmit(false)
        setNewPosition(null)
        getData()
    }

    const openDell = (data) => {
        setValue('positionId', data.id)
        setShowDelete(true)
    }

    const getData = async () => {
        const response = await Conect.get(`/company/${token}`)
        if (response.data.data !== null) {
            let values = response.data.data
            setData(values[0])
            setValue('distance', values[0].distance)
            setDistance(values[0].distance)
        } else {
            getData()
        }
        const resJob = await Conect.get(`/job/${token}`)
        const resAccess = await Conect.get(`/access/${token}`)
        if (resJob.data.data !== null && resAccess.data.Level !== null) {
            let valJob = resJob.data.data
            let valAccess = resAccess.data.Level
            if (valJob != null) {
                let table = valJob.map((value, i) => {
                    return (<tr key={i}>
                        <td>{value.position}</td>
                        {valAccess.map((values, e) => {
                            return (
                                value.access_level_id === values.id &&
                                <td key={e}>{values.description}</td>
                            )
                        })}
                        <td><Button onClick={() => { cadJobs(value, 0) }} variant='outline-warning' style={{ borderRadius: 45 }}><FaPencilAlt /></Button></td>
                        <td><Button onClick={() => { openDell(value) }} variant='outline-danger' style={{ borderRadius: 45 }}><BiTrash /></Button></td>
                    </tr>)
                })
                setJobs(table)
            }
        } else {
            getData()
        }
        if (resJob.data.data !== null && resAccess.data.Level !== null) {
            let valAccess = resAccess.data.Level
            if (valAccess != null) {
                let accessList = valAccess.map((value, i) => {
                    return (
                        <option key={i} value={value.id}>{value.description}</option>
                    )
                })
                setListAccess(accessList)
            }
        }
    }

    const cadJobs = async (data, type) => {
        if (type === 0) {
            setValue('positionId', data.id)
            setValue('position', data.position)
            setValue('access', data.access_level_id)
            setShow(true)
        } else {
            setCad(true)
            setValue('position', '')
            setValue('access', '2606')
            setShow(true)
        }
    }

    const sendCad = async (data) => {
        if (cad === true) {
            const response = await Conect.post(`/job/${token}`, {
                position: data.position,
                access_level_id: data.access
            })
            if (response.data.erro === false) {
                NotificationsAlert("success", response.data.msg, "")
                getData()
                close()
            } else {
                NotificationsAlert("warning", response.data.msg, "")
                close()
            }
        } else {
            const response = await Conect.put(`/jobedit/${token}`, {
                id: data.positionId,
                position: data.position,
                access_level_id: data.access
            })
            if (response.data.erro === false) {
                NotificationsAlert("success", response.data.msg, "")
                getData()
                close()
            } else {
                NotificationsAlert("warning", response.data.msg, "")
                close()
            }
        }
    }
    const sendRemove = async () => {
        const response = await Conect.delete(`/delljob/${token}/${watch("positionId")}`)
        console.log(response)
        if (response.data.erro === false) {
            NotificationsAlert("success", response.data.msg, "")
            getData()
            close()
        } else {
            NotificationsAlert("warning", response.data.msg, "")
            close()
        }
    }

    return (
        <div style={{
            width: '100%',
            height: '80%',
            backgroundColor: "#272E48"

        }}>
            <Modal show={showDelete}>
                <Modal.Header>
                    <Modal.Title>Remover o Cargo?</Modal.Title>
                </Modal.Header>
                <Modal.Footer>
                    <Button variant="primary" style={{ backgroundColor: "#354787" }} onClick={() => { sendRemove() }}>
                        Confirmar
                    </Button>
                    <Button variant="secondary" onClick={() => { close() }}>
                        Cancelar
                    </Button>
                </Modal.Footer>
            </Modal>
            <Modal
                aria-labelledby="contained-modal-title-vcenter"
                centered
                dialogClassName="modal-90w"
                show={show}>

                <form onSubmit={handleSubmit(sendCad)}>
                    <Modal.Header>
                        <Modal.Body >
                            <Container
                            >
                                {cad === false ?
                                    <Row>
                                        <Modal.Title style={{ marginBottom: '2%' }} id="contained-modal-title-vcenter">
                                            Gerenciamento de Cargos.
                                        </Modal.Title>
                                    </Row>
                                    :
                                    <Row>
                                        <Modal.Title style={{ marginBottom: '2%' }} id="contained-modal-title-vcenter">
                                            Cadastro de Cargos.
                                        </Modal.Title>
                                    </Row>
                                }
                                <Row>
                                    <Col>
                                        <InputGroup className="mb-3">
                                            <InputGroup.Text>Decrição</InputGroup.Text>
                                            <Form.Control placeholder='Informe a Descrição do Cargo'

                                                {...register("position")}
                                            />
                                        </InputGroup>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col>
                                        <InputGroup className="mb-3">
                                            <InputGroup.Text>Nivel de Acesso</InputGroup.Text>
                                            <Form.Select {...register("access")} >
                                                <option value="2606">Selecione</option>
                                                {listAccess}
                                            </Form.Select>
                                        </InputGroup>
                                    </Col>
                                </Row>

                            </Container>
                        </Modal.Body>
                    </Modal.Header>
                    <Modal.Footer>
                        <Button
                            type="submit"
                            variant="primary"
                            style={{ backgroundColor: "#354787" }}
                            onSubmit={() => { handleSubmit(sendCad) }}>
                            Confirmar
                        </Button>
                        <Button variant="secondary" onClick={() => { close() }}>
                            Cancelar
                        </Button>
                    </Modal.Footer>
                </form>
            </Modal>
            <div style={{
                margin: 2,
                justifyContent: 'center',
                alignItems: 'center',
                marginBottom: 20,
                marginTop: 15,
                width: "99%",
                height: '99%'
            }}>
                <div style={{
                    marginLeft: 20,
                    display: 'flex',
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'center',

                }}>
                    <h3 style={{ color: '#fff', marginRight: 5 }}>Cadastros da Empresa</h3>
                    <AiFillSetting style={{ fontSize: 30, color: '#fff', marginTop: -5 }} />
                </div>

                <div style={{
                    display: 'flex',
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'center',
                    width: "100%",
                    height: '100%'
                }}>

                    <div style={{ backgroundColor: '#354787', width: '100%', height: "100%", borderRadius: 10 }}>
                        <Container style={{
                            display: 'flex',
                            width: "100%",
                            height: '100%'
                        }}>
                            <Row style={{ width: '100%', height: '100%' }}>
                                <Col xs={8} style={{
                                    width: "100%",
                                    height: "100%",
                                    textAlign: 'center',
                                    // marginLeft: '10%'
                                }}>
                                    <div style={{
                                        display: 'flex',
                                        flexDirection: 'row',
                                        padding: '1%',
                                        borderRadius: 10,
                                        backgroundColor: '#272E48',
                                        marginTop: '1%',
                                        color: "#fff",
                                        justifyContent: 'space-evenly',
                                        alignItems: 'center'
                                    }}>
                                        <h4>Cadastro de Cargos</h4>
                                        <Button
                                            onClick={() => { cadJobs(1, 1) }}
                                            variant="outline-success"
                                            style={{ borderRadius: 45, }}>
                                            <text>Novo Cargo</text>
                                            <AiOutlinePlus style={{ marginBottom: 1, marginLeft: 5 }} />
                                        </Button>
                                    </div>
                                    <Element name="test7" className="element" id="containerElement" style={{
                                        position: 'relative',
                                        height: '85%',
                                        width: '100%',
                                        overflow: 'scroll',
                                    }}>
                                        <Table style={{ color: "#fff" }} bordered >
                                            <thead>
                                                <tr>
                                                    <th>Cargo</th>
                                                    <th>Nivel de Acesso</th>
                                                    <th>Editar</th>
                                                    <th>Remover</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {jobs}
                                            </tbody>
                                        </Table>
                                    </Element>
                                </Col>
                                <Col xs={4} style={{
                                    width: "100%",
                                    height: "100%",
                                    textAlign: 'center',
                                }}>
                                    <h4 style={{
                                        padding: '2%',
                                        borderRadius: 10,
                                        backgroundColor: '#272E48',
                                        marginTop: '2%',
                                        color: "#fff"
                                    }}>Localização Da Empresa</h4>
                                    {isLoaded === true &&
                                        <GoogleMap
                                            mapContainerStyle={{ width: '100%', height: "50%" }}
                                            center={{ lat: data.lat, lng: data.lon }}
                                            zoom={18}
                                            onClick={((e) => newLocation(e.latLng.lat(), e.latLng.lng()))}

                                        // options={{ styles: mapStyles }}

                                        >
                                            {edit === false ?
                                                <Marker
                                                    position={{ lat: data.lat, lng: data.lon }}
                                                    icon={{
                                                        url: pin,
                                                        scaledSize: new window.google.maps.Size(35, 35)
                                                    }}
                                                />
                                                :
                                                submit === true &&
                                                <Marker
                                                    position={{ lat: data.lat, lng: data.lng }}
                                                    icon={{
                                                        url: pin,
                                                        scaledSize: new window.google.maps.Size(35, 35)
                                                    }}
                                                />
                                            }
                                            {edit === false &&
                                                <Circle
                                                    center={{ lat: data.lat, lng: data.lon }}
                                                    radius={distance}
                                                    options={{
                                                        strokeColor: '#023F8E',
                                                        strokeOpacity: 0.5,
                                                        strokeWeight: 2,
                                                        fillColor: '#023F8E',
                                                        fillOpacity: 0.35,
                                                        clickable: false,
                                                        draggable: false,
                                                        editable: false,
                                                    }}
                                                />}

                                        </GoogleMap>
                                    }
                                    {
                                        submit === true ?
                                            <form onSubmit={handleSubmit(onSubmit)}>
                                                <div style={{
                                                    padding: '2%',
                                                    borderRadius: 10,
                                                    backgroundColor: '#272E48',
                                                    marginTop: '2%',
                                                    color: "#fff",

                                                }}>
                                                    <p>{`Raio: ${watch('distance')}m`}</p>
                                                    <Form.Range {...register("distance")} />
                                                </div>

                                                <Button
                                                    style={{
                                                        width: '100%',
                                                        height: '8%',
                                                        marginTop: '2%',
                                                        borderRadius: 10,
                                                        backgroundColor: '#272E48'
                                                    }}
                                                    type="submit"
                                                    onSubmit={() => { handleSubmit(onSubmit) }}>Salvar</Button>
                                                <Button
                                                    style={{
                                                        width: '100%',
                                                        height: '8%',
                                                        marginTop: '2%',
                                                        borderRadius: 10,
                                                        backgroundColor: '#272E48'
                                                    }}
                                                    onClick={() => { close() }}>Cancelar</Button>
                                            </form>
                                            :
                                            edit === false ?
                                                <Button style={{
                                                    width: '100%',
                                                    height: '8%',
                                                    marginTop: '2%',
                                                    borderRadius: 10,
                                                    backgroundColor: '#272E48'
                                                }}
                                                    onClick={() => { editLocation() }}
                                                >Alterar Posição</Button>
                                                :
                                                <Button style={{
                                                    width: '100%',
                                                    height: '8%',
                                                    marginTop: '2%',
                                                    borderRadius: 10,
                                                    backgroundColor: '#272E48'
                                                }}
                                                    onClick={() => { close() }}
                                                >Cancelar</Button>
                                    }
                                </Col >
                                {/* <Col></Col> */}

                            </Row>
                        </Container>
                    </div>
                </div>
            </div>
        </div >
    );
}

export default CadCompany;