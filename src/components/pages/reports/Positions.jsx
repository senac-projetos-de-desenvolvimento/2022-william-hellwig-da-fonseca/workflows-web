import { GoogleMap, Marker, useJsApiLoader, InfoWindow, Polyline } from '@react-google-maps/api';
import { Button, Col, Container, Form, InputGroup, Row } from 'react-bootstrap';
import React, { useEffect, useState } from 'react';
import DateTimePicker from 'react-datetime-picker';
import { AiOutlineClose, AiFillPrinter } from "react-icons/ai";
import { useNavigate } from 'react-router-dom';
import { BiSearchAlt } from "react-icons/bi";
import { useForm } from 'react-hook-form';

import NotificationsAlert from '../../services/NotificationsAlert';
import Conect from '../../../Api/Conect';
import pin from "../../../assets/marker.png";
import moment from 'moment/moment';

import '../../css/style.css';

const Positions = () => {
    const token = localStorage.getItem("token")
    const history = useNavigate()
    const { isLoaded } = useJsApiLoader({ id: 'google-map-script', googleMapsApiKey: "AIzaSyA-S2vsMYvxkccVcjdk5tC0yXQDwdq7_ZM" })
    const [centerPositions, setCenterPositions] = useState({ lat: -31.734183399260882, lng: -52.33417234138075, zoom: 12 })
    const [selectedPosition, setSelectedPosition] = useState(null)
    const [poli, setPoli] = useState(null)
    const [position, setPosition] = useState(null)
    const [positions, setPositions] = useState(null)
    const [impress, setImpress] = useState(false)
    const { register, handleSubmit, setValue, watch } = useForm()
    const [date, setDate] = useState(new Date());

    const options = {
        strokeColor: "#354787",
        strokeOpacity: 1,
        strokeWeight: 2,
    }
    useEffect(() => {
        if (token === null) {
            NotificationsAlert("warning", "Desconectado", "Faça login para continuar!")
            history("/login");
        } else {
            getUsers()
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const getUsers = async () => {
        const response = await Conect.get(`/user/${token}`)
        if (response.data.Users != null) {
            let data = response.data.Users.map((value, index) => {
                return (
                    value.location === 1 && <option key={index} value={value.id}>{value.name}</option>
                )
            })
            setPositions(data)
        }
    }
    const sendImpress = async () => {
        if (watch('user_id') === 2606) {
            NotificationsAlert("warning", "Selecione Um Usuario!", "")
        } else {
            const response = await Conect.post(`/reportpositionstopdf/${token}`, {
                user_id: watch('user_id'),
                date: moment(date).format("YYYY-MM-DD")
            })
            if (response.data.erro === true) {
                NotificationsAlert("warning", "Atenção", response.data.msg)
            } else {
                if (response.data.URL) {
                    NotificationsAlert("info", "Gerando Pdf", 'Aguarde...')
                    setTimeout(() => {
                        window.open(response.data.URL, '_blank');
                    }, 3000);
                }
            }
        }
    }
    const onSubmit = async (data) => {
        setCenterPositions({ lat: -31.734183399260882, lng: -52.33417234138075, zoom: 12 })

        if (data.user_id === 2606) {
            NotificationsAlert("warning", "Selecione Um Usuario!", "")
        } else {
            const response = await Conect.post(`/reportpositions/${token}`, {
                user_id: data.user_id,
                date: moment(date).format("YYYY-MM-DD")
            })
            if (response.data.erro === true) {
                NotificationsAlert("warning", "Atenção", response.data.msg)
            }
            else {
                let data = response.data.data
                if (response.data.data != null) {
                    var dados = data.map((value, index) => {
                        return (<Marker
                            key={index}
                            position={{ lat: value.lat, lng: value.long }}
                            onClick={() => {
                                setSelectedPosition(value)
                                setCenterPositions({ lat: value.lat, lng: value.long, zoom: 15 })
                            }}
                            options={{
                                label: {
                                    text: `${index + 1}`,
                                    className: "map-marker"
                                }
                            }}
                            icon={{
                                url: pin,
                                scaledSize: new window.google.maps.Size(35, 35)
                            }}
                        />
                        )
                    })
                    var poli = data.map((value, index) => {
                        return ({
                            lat: value.lat,
                            lng: value.long
                        })
                    })
                }
                setPosition(dados)
                setPoli(poli)
                if (dados.length) {
                    setImpress(true)
                }
            }
        }
    }
    const clear = () => {
        setValue("user_id", 2606)
        setPoli([])
        setPosition(null)
        setDate(new Date())
        setImpress(false)
        setCenterPositions({ lat: -31.734183399260882, lng: -52.33417234138075, zoom: 12 })
    }

    return (
        <div style={{
            width: '99vw',

        }}>
            <div style={{
                margin: 2,
                justifyContent: 'center',
                alignItems: 'center',
                marginBottom: 20,
                width: '100%',
                marginTop: 10,
            }}>
                <Container style={{
                    width: '100vw',
                }}>
                    <form onSubmit={handleSubmit(onSubmit)}>
                        <Row >
                            <Col >
                                <h3 style={{ color: '#fff' }}>Posições</h3>
                            </Col>
                            <Col
                                xs={6}
                                style={{
                                    display: 'flex',
                                    alignItems: "center",
                                    backgroundColor: '#fff',
                                    borderRadius: 45,
                                    marginLeft: '-10%'
                                }}>
                                <InputGroup
                                    style={{ paddingLeft: '1%' }}
                                ><InputGroup.Text>Data</InputGroup.Text>
                                    <DateTimePicker
                                        disableClock={true}
                                        format='dd-MM-y'
                                        onChange={setDate}
                                        value={date}
                                    />
                                </InputGroup>
                                <InputGroup
                                    style={{ marginRight: '1%' }}
                                >
                                    <InputGroup.Text style={{ marginLeft: -10 }}>Usuario</InputGroup.Text>
                                    <Form.Select {...register("user_id")} >
                                        <option value={2606}>Selecione</option>
                                        {positions}
                                    </Form.Select>
                                </InputGroup>
                            </Col>
                            <Col >
                                <Button
                                    style={{ width: "20%", borderRadius: 45 }}
                                    variant="outline-success"
                                    type="submit" onSubmit={() => { handleSubmit(onSubmit) }}
                                >
                                    <BiSearchAlt style={{ marginRight: 3, marginBottom: 3 }} />
                                </Button>
                                <Button
                                    style={{ width: "20%", borderRadius: 45, marginLeft: 5, color: '#000' }}
                                    variant="outline-secondary"
                                    onClick={() => { clear() }}
                                >
                                    <AiOutlineClose style={{ marginRight: 3, marginBottom: 3, color: '#fff' }} />
                                </Button>
                                {impress === true &&
                                    <Button
                                        style={{ width: "20%", borderRadius: 45, marginLeft: 5 }}
                                        variant="outline-light"
                                        onClick={() => { sendImpress() }}
                                    >
                                        <AiFillPrinter style={{ marginRight: 3, marginBottom: 3 }} />
                                    </Button>}
                            </Col>
                        </Row>
                    </form>
                </Container>
            </div>
            <div style={{
                display: 'flex',
                flexWrap: 'wrap',
                justifyContent: 'center',
                flexDirection: 'row',
            }}>
                <div style={{
                    backgroundColor: '#354787',
                    width: "99%",
                    height: "78vh",
                    marginLeft: '1%'

                }}>
                    {isLoaded ? (
                        <GoogleMap
                            mapContainerStyle={{ width: '100%', height: "100%" }}
                            center={{ lat: centerPositions.lat, lng: centerPositions.lng }}
                            zoom={centerPositions.zoom}
                        // options={{ styles: mapStyles }}
                        >
                            {position != null && position}
                            <Polyline options={options} path={poli} />
                            {selectedPosition && (
                                <InfoWindow
                                    position={{
                                        lat: selectedPosition.lat,
                                        lng: selectedPosition.long
                                    }}
                                    onCloseClick={() => {
                                        setSelectedPosition(null)
                                    }}

                                >
                                    <div>
                                        <h3>{selectedPosition.name}</h3>
                                        <h5 style={{ fontStyle: 'oblique' }}>{`${selectedPosition.position}`}</h5>
                                        <p>{`Velocidade: ${selectedPosition.speed.toFixed(2)}Km/h`}</p>
                                        <p>{`Dia: ${moment(selectedPosition.created_at).format("DD/MM/YYYY")} Hora: ${moment(selectedPosition.created_at).format("H:mm:ss")}`}</p>

                                    </div>
                                </InfoWindow>
                            )}
                        </GoogleMap>
                    ) : <></>}

                </div>
            </div>
        </div>
    );
}

export default Positions;