import { Button, Col, Container, Form, InputGroup, Row, Table } from 'react-bootstrap';
import React, { useEffect, useState } from 'react';
import DateTimePicker from 'react-datetime-picker';
import { BsCheck2, BsDash } from "react-icons/bs";
import { AiOutlineClose, AiFillPrinter } from "react-icons/ai";
import { useNavigate } from 'react-router-dom';
import { BiSearchAlt } from "react-icons/bi";
import { useForm } from 'react-hook-form';
import { Element } from 'react-scroll';


import NotificationsAlert from '../../services/NotificationsAlert';
import Conect from '../../../Api/Conect';
import moment from 'moment/moment';

import '../../css/style.css';

const WorkPoint = () => {
    const token = localStorage.getItem("token")
    const history = useNavigate()
    const [data, setData] = useState([])
    const [positions, setPositions] = useState(null)
    const [impress, setImpress] = useState(false)
    const { register, handleSubmit, setValue, watch } = useForm()
    const [date, setDate] = useState(new Date());

    useEffect(() => {
        if (token === null) {
            NotificationsAlert("warning", "Desconectado", "Faça login para continuar!")
            history("/login");
        } else {
            getData()
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const getUsers = async () => {
        const response = await Conect.get(`/user/${token}`)
        if (response.data.Users != null) {
            let data = response.data.Users.map((value, index) => {
                return (<option key={index} value={value.id}>{value.name}</option>)
            })
            setPositions(data)
        }
    }
    const getData = async () => {
        const response = await Conect.get(`/lastpoint/${token}`)
        if (response.data.data !== null) {
            let values = response.data.data
            let dados = values.map((value, index) => {
                return (<tr style={{ color: "#fff" }} key={index}>
                    <td>{value.workpoint === 0 ?
                        <div style={{ color: '#bf740e' }} >
                            <BsCheck2 style={{ fontSize: 30 }} />
                            Entrada
                        </div>
                        :
                        <div style={{ color: '#C0C0C0' }} >
                            <BsDash style={{ fontSize: 30 }} />
                            Saida
                        </div>
                    }</td>
                    <td>{value.name}</td>
                    <td>{moment(value.created_at).format('DD/MM/YY')}</td>
                    <td>{moment(value.created_at).format('HH:mm:ss')}</td>
                </tr>)
            })
            setData(dados)
        }
        getUsers()
    }
    const onSubmit = async (data) => {
        const response = await Conect.post(`/userPoint/${token}`, {
            user_id: data.user_id,
            date: moment(date).format("YYYY-MM-DD") === 'Invalid date' ? null: moment(date).format("YYYY-MM-DD")
        })
        if (response.data != null) {
            if (response.data.erro === false) {
                if (response.data.data != null) {
                    let dados = response.data.data.map((value, index) => {
                        return (<tr style={{ color: "#fff" }} key={index}>
                            <td>{value.workpoint === 0 ?
                                <div style={{ color: '#bf740e' }} >
                                    <BsCheck2 style={{ fontSize: 30 }} />
                                    Entrada
                                </div>
                                :
                                <div style={{ color: '#C0C0C0' }} >
                                    <BsDash style={{ fontSize: 30 }} />
                                    Saida
                                </div>
                            }</td>
                            <td>{value.name}</td>
                            <td>{moment(value.created_at).format('DD/MM/YY')}</td>
                            <td>{moment(value.created_at).format('HH:mm:ss')}</td>
                        </tr>)
                    })
                    setData(dados)
                    if (dados.length) {
                        setImpress(true)
                    }
                } else {
                    getData()
                }
            } else {
                NotificationsAlert("warning", response.data.msg, "")
            }
        }
    }
    const clear = () => {
        setData([])
        getData()
        setImpress(false)
        setDate(new Date())
        setValue("user_id", 2606)
    }
    const sendImpress = async () => {

        const response = await Conect.post(`/reportworkpointtopdf/${token}`, {
            user_id: watch('user_id'),
            date: moment(date).format("YYYY-MM-DD")
        })
        if (response.data.erro === true) {
            NotificationsAlert("warning", "Atenção", response.data.msg)
        } else {
            if (response.data.URL) {
                NotificationsAlert("info", "Gerando Pdf", 'Aguarde...')
                setTimeout(() => {
                    window.open(response.data.URL, '_blank');
                }, 3000);
            }
        }
    }

    return (
        <div style={{
            width: '99vw',

        }}>
            <div style={{
                margin: 2,
                justifyContent: 'center',
                alignItems: 'center',
                marginBottom: 20,
                width: '100%',
                marginTop: 10,
            }}>
                <Container style={{
                    width: '100vw',
                }}>
                    <form onSubmit={handleSubmit(onSubmit)}>
                        <Row >
                            <Col style={{ marginLeft: '-5%' }}>
                                <h3 style={{ color: '#fff' }}>Pontos de Trabalho</h3>
                            </Col>
                            <Col
                                xs={6}
                                style={{
                                    display: 'flex',
                                    alignItems: "center",
                                    backgroundColor: '#fff',
                                    borderRadius: 45,
                                    marginLeft: '-5%'
                                }}>
                                <InputGroup
                                    style={{ paddingLeft: '1%' }}
                                ><InputGroup.Text>Data</InputGroup.Text>
                                    <DateTimePicker
                                        disableClock={true}
                                        format='dd-MM-y'
                                        onChange={setDate}
                                        value={date}
                                    />
                                </InputGroup>
                                <InputGroup
                                    style={{ marginRight: '1%' }}
                                >
                                    <InputGroup.Text style={{ marginLeft: -10 }}>Usuario</InputGroup.Text>
                                    <Form.Select {...register("user_id")} >
                                        <option value="2606">Todos</option>
                                        {positions}
                                    </Form.Select>
                                </InputGroup>
                            </Col>
                            <Col >
                                <Button
                                    style={{ width: "20%", borderRadius: 45 }}
                                    variant="outline-success"
                                    type="submit" onSubmit={() => { handleSubmit(onSubmit) }}
                                >
                                    <BiSearchAlt style={{ marginRight: 3, marginBottom: 3 }} />
                                </Button>
                                <Button
                                    style={{ width: "20%", borderRadius: 45, marginLeft: 5, color: '#000' }}
                                    variant="outline-secondary"
                                    onClick={() => { clear() }}
                                >
                                    <AiOutlineClose style={{ marginRight: 3, marginBottom: 3, color: '#fff' }} />
                                </Button>
                                {impress === true &&
                                    <Button
                                        style={{ width: "20%", borderRadius: 45, marginLeft: 5 }}
                                        variant="outline-light"
                                        onClick={() => { sendImpress() }}
                                    >
                                        <AiFillPrinter style={{ marginRight: 3, marginBottom: 3 }} />
                                    </Button>}
                            </Col>
                        </Row>
                    </form>
                </Container>
            </div>
            <div style={{
                display: 'flex',
                flexWrap: 'wrap',
                justifyContent: 'center',
                flexDirection: 'row',
            }}>
                <div style={{
                    backgroundColor: '#354787',
                    width: "97%",
                    height: "75vh",
                    marginLeft: '1%'

                }}>
                    <Element name="test7" className="element" id="containerElement" style={{
                        // position: 'relative',
                        height: '99%',
                        width: '100%',
                        overflow: 'scroll',
                    }}>
                        <Table >
                            <thead>
                                <tr style={{ color: '#fff' }}>
                                    <th><text>Ponto</text></th>
                                    <th><text>Nome</text></th>
                                    <th><text>Data</text></th>
                                    <th><text>Hora</text></th>
                                </tr>
                            </thead>

                            <tbody>
                                {data}
                            </tbody>
                        </Table>
                    </Element>
                </div>
            </div>
        </div>
    );
}

export default WorkPoint;