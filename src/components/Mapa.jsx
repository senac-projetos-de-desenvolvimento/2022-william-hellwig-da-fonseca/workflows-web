import { GoogleMap, Marker, useJsApiLoader, InfoWindow } from '@react-google-maps/api';
import React, { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import moment from 'moment/moment';

import NotificationsAlert from './services/NotificationsAlert';
import pin from "../assets/marker.png";
import Conect from '../Api/Conect';

import mapStyles from './css/mapa.js';
import './css/style.css';

const Mapa = () => {
    const history = useNavigate();
    const token = localStorage.getItem("token")
    const [positions, setPositions] = useState([])
    const [centerPositions, setCenterPositions] = useState({ lat: -31.769899294545382, lng: -52.340982621306615, zoom: 12 })
    const [selectedPosition, setSelectedPosition] = useState(null)
    const { isLoaded } = useJsApiLoader({
        id: 'google-map-script',
        googleMapsApiKey: "AIzaSyA-S2vsMYvxkccVcjdk5tC0yXQDwdq7_ZM"
    })


    useEffect(() => {
        if (token === null) {
            NotificationsAlert("warning", "Desconectado", "Faça login para continuar!")
            history("/login");
        } else {
            setInterval(() => {
                getPositions()
            }, 30000);
        }
        getPositions()
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const getPositions = async () => {
        const response = await Conect.get(`/lastpositions/${token}`)
        if (response.data.erro === true) {
            NotificationsAlert("warning", "Atenção", response.data.msg)
        }
        else {
            let data = response.data.data
            if (response.data.data != null) {
                var dados = data.map((value, index) => {
                    return (<Marker
                        key={index}
                        position={{ lat: value.lat, lng: value.long }}
                        onClick={() => {
                            setSelectedPosition(value)
                            setCenterPositions({ lat: value.lat, lng: value.long, zoom: 15 })
                        }}
                        options={{
                            label: {
                                text: `${value.name}`,
                                className: "map-marker"
                            }
                        }}
                        icon={{
                            url: pin,
                            scaledSize: new window.google.maps.Size(35, 35)
                        }}
                    />)
                })
            }
            setPositions(dados)
        }
    }

    return <div style={{ width: "100%", height: "88%" }}>
        {isLoaded ? (
            <GoogleMap
                mapContainerStyle={{ width: '100%', height: "100%" }}
                center={{ lat: centerPositions.lat, lng: centerPositions.lng }}
                zoom={centerPositions.zoom}
                options={{ styles: mapStyles }}
            >
                {positions != null && positions}

                {selectedPosition && (
                    <InfoWindow
                        position={{
                            lat: selectedPosition.lat,
                            lng: selectedPosition.long
                        }}
                        onCloseClick={() => {
                            setSelectedPosition(null)
                            setCenterPositions({ lat: -31.769899294545382, lng: -52.340982621306615, zoom: 12 })
                        }}

                    >
                        <div>
                            <h3>{selectedPosition.name}</h3>
                            <h5 style={{ fontStyle: 'oblique' }}>{`${selectedPosition.position}`}</h5>
                            <p>{`Velocidade: ${selectedPosition.speed.toFixed(2)}Km/h`}</p>
                            <p>{`Dia: ${moment(selectedPosition.created_at).format("DD/MM/YYYY")} Hora: ${moment(selectedPosition.created_at).format("H:mm:ss")}`}</p>

                        </div>
                    </InfoWindow>
                )}
            </GoogleMap>
        ) : <></>}
    </div>

}

export default Mapa;