import { useNavigate } from "react-router-dom";
import React, { useContext } from "react";
import { useForm } from "react-hook-form";
import Image from 'react-bootstrap/Image';

import NotificationsAlert from "./services/NotificationsAlert";
import { ClienteContext } from "./services/ClienteContext";
import Logo from '../assets/logo.png';
import Conect from "../Api/Conect";


const UserLogin = () => {

    const history = useNavigate();
    const { register, handleSubmit, } = useForm();
    const cliente = useContext(ClienteContext);
    const onSubmit = async (data) => {

        const usuario = await Conect.post("/login", {
            email: data.email,
            password: data.password
        })
        if (usuario.data.erro === true) {
            if (usuario.data.status === true) {
                NotificationsAlert("danger", "Atenção", usuario.data.msg)
            } else {
                NotificationsAlert("warning", "Atenção", usuario.data.msg)
            }
        }
        else {
            localStorage.setItem("token", usuario.data.data.token)
            localStorage.setItem("name", usuario.data.data.name)
            localStorage.setItem("user_id", usuario.data.data.user_id)
            localStorage.setItem("position", usuario.data.data.position)
            localStorage.setItem("image", usuario.data.data.image)


            cliente.setDados({
                user_id: usuario.data.data.user_id,
                name: usuario.data.data.name,
                token: usuario.data.data.token,
                position: usuario.data.data.position,
                image: usuario.data.data.image
            });
            let message = `Olá! ${usuario.data.data.name}!`
            NotificationsAlert("default", message, "")
            history("/home");
        }
    };

    return (
        <div style={{ width: '100vw', display: "flex", justifyContent: "center", alignItems: "center" }}>
            <form className="form-signin" style={{ width: '30vw', height: '50vh', marginTop: 70 }} onSubmit={handleSubmit(onSubmit)}>
                <div className="text-center mb-4">
                    <Image style={{ marginRight: 20 }} src={Logo} />
                </div>
                <div className="form-label-group" style={{ marginBottom: 10 }}>
                    <input type="email" id="email" className="form-control" placeholder="Informe Seu Email" required autoFocus {...register("email")} />
                </div>
                <div className="form-label-group" style={{ marginBottom: 10 }}>
                    <input type="password" id="password" className="form-control" placeholder="Informe Sua Senha" required {...register("password")} />
                </div>
                <button className="btn btn-md btn-primary btn-block" style={{ backgroundColor: '#354787', borderRadius: 45 }} type="submit"><text>Login</text></button>
            </form>
        </div>
    )

}

export default UserLogin;
