import { Nav, Container, NavDropdown, Row, Col } from 'react-bootstrap';
import { useNavigate } from "react-router-dom";
import Image from 'react-bootstrap/Image'
import { Link } from "react-router-dom";
import { useContext } from "react";

import { ClienteContext } from "./services/ClienteContext";
import Logo from '../assets/Icon WF.png'

const Header = () => {

    const cliente = useContext(ClienteContext);
    const history = useNavigate();

    const loginLogout = () => {
        cliente.setDados({ id: null, nome: "", token: "" });
        localStorage.clear()
        history("/login")
    }

    let user;
    if (cliente.dados.user_id) {
        user = (
            <>
                <text className="btn" style={{ color: '#fff' }}>Olá {cliente.dados.name} <i className="fas fa-sign-out-alt" /></text>
            </>
        )
    } else {
        user = (
            <>
                <text className="btn" style={{ color: '#fff' }}>Entrar <i className="fas fa-sign-in-alt"></i></text>
            </>
        )
    }

    return (
        <Nav style={{ backgroundColor: '#354787' }}>
            <Container >
                <Row style={{ alignItems: 'center', justifyContent: 'center', marginTop: 5, marginBottom: 5 }}>
                    <Col style={{ marginLeft: '-20vh' }}>
                        <Image style={{ width: 60, height: 60, marginRight: 20 }} src={Logo} />
                        <Link className="navbar-brand" to="/home">
                            <text style={{ color: '#fff' }}>Work Flows Sistema</text>
                        </Link>
                    </Col>
                    <Col >
                        <Nav className="me-auto">
                            <Nav.Link><Link to="/mapa" style={{ color: '#fff' }}>Mapa</Link></Nav.Link>
                            <NavDropdown style={{ color: '#fff' }} title={<text style={{ color: '#fff' }}>Relatórios</text>} id="basic-nav-dropdown" >
                                <div style={{ backgroundColor: '#354787', marginTop: -10, marginBottom: -10 }}>
                                    <NavDropdown.Item style={{ backgroundColor: '#354787' }}>
                                        <Link to="/workpoint" style={{ color: '#fff' }}>Pontos</Link>
                                    </NavDropdown.Item>
                                    <NavDropdown.Item style={{ backgroundColor: '#354787' }}>
                                        <Link to="/positions" style={{ color: '#fff' }}>Posições</Link>
                                    </NavDropdown.Item>
                                    {/* <NavDropdown.Item style={{ backgroundColor: '#354787' }}>
                                        <Link to="/relgrafics" style={{ color: '#fff' }}>Gráficos</Link>
                                    </NavDropdown.Item>
                                    <NavDropdown.Item style={{ backgroundColor: '#354787' }}>
                                        <Link to="/" style={{ color: '#fff' }}>Localização</Link>
                                    </NavDropdown.Item> */}
                                </div>
                            </NavDropdown>
                            <NavDropdown title={<text style={{ color: '#fff' }}>Empresa</text>} id="basic-nav-dropdown" style={{ color: '#fff' }} >
                                <div style={{ backgroundColor: '#272E48', marginTop: -10, marginBottom: -10 }}>
                                    <NavDropdown.Item style={{ backgroundColor: '#354787' }}>
                                        <Link to="/cadwarnings" style={{ color: '#fff' }}>Avisos</Link>
                                    </NavDropdown.Item>
                                    <NavDropdown.Item style={{ backgroundColor: '#354787' }}>
                                        <Link to="/cadvags" style={{ color: '#fff' }}>Vagas</Link>
                                    </NavDropdown.Item>
                                    <NavDropdown.Divider />
                                    <NavDropdown.Item style={{ backgroundColor: '#354787' }}>
                                        <Link to="/cadcomp" style={{ color: '#fff' }}>Cadastros</Link>
                                    </NavDropdown.Item>
                                </div>
                            </NavDropdown>
                            <NavDropdown title={<text style={{ color: '#fff' }}>Colaboradoes</text>} id="basic-nav-dropdown" style={{ color: '#fff' }} >
                                <div style={{ backgroundColor: '#272E48', marginTop: -10, marginBottom: -10 }}>
                                    <NavDropdown.Item style={{ backgroundColor: '#354787' }}>
                                        <Link to="/cadusers" style={{ color: '#fff' }}>Cadastros</Link>
                                    </NavDropdown.Item>
                                    <NavDropdown.Item style={{ backgroundColor: '#354787' }}>
                                        <Link to="/listscore" style={{ color: '#fff' }}>Ranking</Link>
                                    </NavDropdown.Item>
                                    <NavDropdown.Item style={{ backgroundColor: '#354787' }}>
                                        <Link to="/reports" style={{ color: '#fff' }}>Denuncias</Link>
                                    </NavDropdown.Item>
                                </div>
                            </NavDropdown>
                        </Nav>
                    </Col>
                    <Col xs lg="3" style={{ marginRight: -30 }}>
                        <Link to="/login" onClick={loginLogout}>{user}</Link>
                    </Col>
                </Row>
            </Container>
        </Nav >
    );
};



export default Header;