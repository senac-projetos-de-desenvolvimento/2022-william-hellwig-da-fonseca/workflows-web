import { useNavigate } from 'react-router-dom';
import React, { useEffect } from 'react'

import NotificationsAlert from './services/NotificationsAlert';

const Home = () => {
  const token = localStorage.getItem("token")
  const history = useNavigate()

  useEffect(() => {
    if (token === null) {
      NotificationsAlert("warning", "Desconectado", "Faça login para continuar!")
      history("/login");
    }
  });
  return (
    <div className="row" style={{ width: '100vw' }}>
      <h1 style={{ marginLeft: 20, marginTop: 20, color: '#fff' }} >Bem-Vindo ao Sistema Work Flows</h1>
    </div>
  );
}

export default Home;