# WorkFlows - Web :wrench:


## Descrição :page_facing_up:

Esta aplicação foi desenvolvida com o intuito de aplicar-se em toda e qualquer empresa, aquelas em que desejem manter sua estrutura “humana” engajada, motivada e próxima, que de forma eficiente irá trazer o crescimento, lucratividade e entrosamento da organização.

## Instalação do Projeto :construction_worker:

- Vamos iniciar com a Instalação do Node Js. [Obter Node js](https://nodejs.org/en/download/) 
  ````
   Node.js é um software que permite a execução de códigos JavaScript fora de um navegador web.
  ````
- O Proximo passo é instalar o React .
    ```
    O React é uma biblioteca front-end JavaScript de código aberto com foco em criar interfaces de usuário em páginas web.
    ```
```
npm install --global react
```
- Com tudo devidamente instalado podemos clonar o repositorio.

```
git clone chave ssh. ou https.
```
- Agora entramos no diretorio do projeto e Intalamos as dependencias.
```
npm install
```
- Finalmenre para executar a aplicação.
```
npm start
```

Sucesso :tada:


## Responsaveis :alien:

##### Desenvolvedor/Criador

- [Willian H. Da Fonseca](https://github.com/sakamottosann)
  
##### Orientador 

- [Angelo Luz](https://github.com/angelogluz)